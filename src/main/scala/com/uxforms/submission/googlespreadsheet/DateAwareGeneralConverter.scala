package com.uxforms.submission.googlespreadsheet

import com.uxforms.domain.widget.{DateWidget, Named, Persisted, Widget}
import com.uxforms.domain.{FormData, FormDefinition, RequestInfo}
import com.uxforms.dsl.Form
import play.api.libs.json._

/**
 * Iterates through the form definition and converts each widget's data to a String.
 * Handles DateWidgets as a special case - formats their values as "%d/%02d/%02d".format(year, month, day).
 */
class DateAwareGeneralConverter(widgetNamesToExclude: Set[String] = Set.empty) extends Converter {

  def jsValueToString: JsValue => String = {
    case JsString(s) => s
    case JsBoolean(b) => b.toString
    case JsNumber(n) => n.toString
    case o: JsObject => jsObjectToString(o)
    case JsArray(arr) => jsArrayToString(arr)
    case JsNull => ""
  }

  def jsObjectToString: JsObject => String = _.fields.map(x => jsValueToString(x._2)).mkString(", ")

  def jsArrayToString: Seq[JsValue] => String = _.map(jsValueToString).mkString(", ")

  val candidateWidgets: FormDefinition => Seq[Widget with Named with Persisted] =
    _.flattenWidgets.collect { case w: Named with Persisted if !widgetNamesToExclude.contains(w.name) => w }

  def formatDate(year: Int, month: Int, day: Int): String = "%d/%02d/%02d".format(year, month, day)

  //noinspection RedundantBlock
  def columnValues: (FormData, FormDefinition, RequestInfo) => Seq[String] =
    (data, formDef, _) => candidateWidgets(formDef).map {
      case dw: DateWidget => {
        (data.textData \ dw.name).toOption.fold("")(root =>
          (root \ "year").asOpt[Int].fold("")(year =>
            formatDate(year, (root \ "month").asOpt[Int].getOrElse(1), (root \ "day").asOpt[Int].getOrElse(1))
          )
        )
      }
      case w: Named with Persisted =>
        (data.textData \ w.name).toOption.fold("")(jsValueToString)
      case _ => ""
    }

  override def convert: (Form, RequestInfo) => Row = (form, requestInfo) => {
    Row(
      columnValues(form.data, form.formDefinition, requestInfo)
    )
  }
}
