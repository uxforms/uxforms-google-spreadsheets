package com.uxforms.submission.googlespreadsheet.internal

import java.io.InputStream

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.sheets.v4.model.ValueRange
import com.google.api.services.sheets.v4.{Sheets, SheetsScopes}
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.ServiceAccountCredentials
import com.uxforms.submission.googlespreadsheet.{Row, UserEnteredInputOption, ValueInputOption}

import scala.collection.JavaConverters._

class GoogleSpreadsheetService(googleCredentialsJson: InputStream) {

  private lazy val credentials =
    ServiceAccountCredentials.fromStream(googleCredentialsJson).createScoped(Seq(SheetsScopes.SPREADSHEETS).asJava)

  private def toValueRange(row: Row): ValueRange = {
    val range = new ValueRange()
    range.setValues(Seq(row.values.map(_.asInstanceOf[Object]).asJava).asJava)
  }

  def appendRow(spreadsheetId: String, row: Row, inputOption: ValueInputOption = UserEnteredInputOption): Unit = {
    val sheets = new Sheets.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance, new HttpCredentialsAdapter(credentials))
      .setApplicationName("GoogleSpreadsheetService")
      .build()

    sheets.spreadsheets().values()
      .append(spreadsheetId, "Sheet1!A:A", toValueRange(row))
      .setValueInputOption(inputOption.value)
      .execute()
  }

}
