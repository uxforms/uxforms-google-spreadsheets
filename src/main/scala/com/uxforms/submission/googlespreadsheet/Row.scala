package com.uxforms.submission.googlespreadsheet

/**
 * A row in a Google Spreadsheet.
 *
 * Empty values should be represented as an empty string `""` as `null` values will be ignored.
 */
case class Row(values: Seq[String])
