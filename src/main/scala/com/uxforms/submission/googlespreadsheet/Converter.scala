package com.uxforms.submission.googlespreadsheet

import com.uxforms.domain.RequestInfo
import com.uxforms.dsl.Form

trait Converter {
  def convert: (Form, RequestInfo) => Row
}
