package com.uxforms.submission.googlespreadsheet

import java.io.InputStream

import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.submission.googlespreadsheet.internal.GoogleSpreadsheetService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
 * Submits your form's data to a Google Spreadsheet.
 *
 * The spreadsheet must already exist and your google service account must have permission to write to it.
 *
 * '''NOTE:''' Since version 16.0.0 the spreadsheet no longer requires column headings.
 *
 * Your spreadsheetId can be found within the URL of the sheet by taking the path parameter after `/d/`.
 * E.g. a sheet with the URL `https://docs.google.com/spreadsheets/d/aabbccddee/edit#gid=0` has an id of `aabbccddee`.
 *
 * @param googleCredentialsJson The (json) key from your google service account
 * @param spreadsheetId         Case sensitive.
 * @param convertFormData       A function to convert your form's data into rows to be inserted into the Google spreadsheet.
 *                              See [[DateAwareGeneralConverter.convert]] for a ready-made implementation.
 */
class GoogleSpreadsheetSubmission(googleCredentialsJson: InputStream, spreadsheetId: String,
                                  convertFormData: (Form, RequestInfo) => Row) extends DataTransformer {


  override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] =
    Future {
      new GoogleSpreadsheetService(googleCredentialsJson)
        .appendRow(spreadsheetId, convertFormData(form, requestInfo))
      DataTransformationResult(form.data, None)
    }

}
