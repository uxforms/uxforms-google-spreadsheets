package com.uxforms.submission.googlespreadsheet.internal

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.sheets.v4.model.{ClearValuesRequest, ValueRange}
import com.google.api.services.sheets.v4.{Sheets, SheetsScopes}
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.ServiceAccountCredentials
import com.uxforms.submission.googlespreadsheet.Row
import org.scalatest.{Matchers, WordSpec}

import scala.collection.JavaConverters._

class GoogleSpreadsheetServiceIntegrationSpec extends WordSpec with Matchers {

  // Found in UX Forms' GDrive: /uxforms-google-spreadsheets/IntegrationTest
  private val spreadsheetId = "1FoiuJi41Lmbq_JGE5I2asI_HWtFzEcY_iuDodW11YUo"

  private val credentials = ServiceAccountCredentials.fromStream(getClass.getResourceAsStream("/centered-arbor-259915-93d7fbfe704f.json"))
    .createScoped(Seq(SheetsScopes.SPREADSHEETS).asJava)

  val sheets = new Sheets.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance, new HttpCredentialsAdapter(credentials))
    .setApplicationName("GoogleSpreadsheetService")
    .build()

  def clearSheet(): Unit = {
    sheets.spreadsheets().values()
      .clear(spreadsheetId, "Sheet1!A:Z", new ClearValuesRequest())
      .execute()
  }

  def fetchData(): ValueRange = {
    sheets.spreadsheets().values()
      .get(spreadsheetId, "Sheet1!A:A")
      .execute()
  }

  "appendRow" should {
    "add a new row to the sheet" in {
      clearSheet()
      assume(Option(fetchData().getValues).isEmpty)

      new GoogleSpreadsheetService(getClass.getResourceAsStream("/centered-arbor-259915-93d7fbfe704f.json"))
        .appendRow(spreadsheetId, Row(Seq("Another", "", "test")))

      fetchData().getValues.size() shouldBe 1
    }
  }

}
