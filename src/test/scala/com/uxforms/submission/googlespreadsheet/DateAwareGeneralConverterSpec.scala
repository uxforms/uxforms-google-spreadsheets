package com.uxforms.submission.googlespreadsheet

import java.util.{Locale, UUID}

import com.uxforms.domain._
import com.uxforms.domain.constraint.Constraint
import com.uxforms.domain.widget.{DateWidget, Named, Persisted, Widget}
import com.uxforms.dsl.Form
import com.uxforms.dsl.containers._
import org.joda.time.Instant
import org.scalatest.{FreeSpec, Matchers}
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

class DateAwareGeneralConverterSpec extends FreeSpec with Matchers {

  val converter = new DateAwareGeneralConverter

  "columnValues" - {
    "should return an empty string when a date is not present" in {
      val result = converter.columnValues(FormData(Json.obj("dateOne" -> Json.obj("year" -> 2016))), buildFormDef(Seq(dateWidget("dateOne"), dateWidget("dateTwo"))), requestInfo)
      result(1) shouldBe ""
    }
    "should return Jan or the first of the month if either the month or day is missing" in {
      val result = converter.columnValues(FormData(Json.obj("dateOne" -> Json.obj("year" -> 2016))), buildFormDef(Seq(dateWidget("dateOne"))), requestInfo)
      result.head shouldBe "2016/01/01"
    }
  }

  "jsObjectToString" - {
    "should return an empty string if the object is empty" in {
      converter.jsObjectToString(JsObject(Seq.empty)) shouldBe ""
    }

    "should return a comma separated string of all values" in {
      converter.jsObjectToString(Json.obj("fieldOne" -> "a val", "fieldTwo" -> "anotherVal")) shouldBe "a val, anotherVal"
    }

    "should return a comma separated string of even nested values" in {
      val result = converter.jsObjectToString(
        Json.obj(
          "fieldOne" -> "a val",
          "fieldTwo" -> Json.obj("nestedFieldOne" -> "nested value", "nestedFieldTwo" -> "another nested value"),
          "fieldThree" -> "another val"
        ))

      result shouldBe "a val, nested value, another nested value, another val"
    }
  }

  "jsArrayToString" - {
    "should return an empty string if the array is empty" in {
      converter.jsArrayToString(Seq.empty) shouldBe ""
    }

    "should return a comma separated string of all values" in {
      converter.jsArrayToString(Seq(JsString("first"), JsString("second"), JsString("third"))) shouldBe "first, second, third"
    }
  }

  "formatDate" - {
    "should front-pad months and days" in {
      converter.formatDate(2015, 1, 2) shouldBe "2015/01/02"
    }

    "should leave the year field as given" in {
      converter.formatDate(16, 12, 13) shouldBe "16/12/13"
    }
  }

  "candidateWidgets" - {
    "should only include widgets that are persisted with a name" in {
      val widgets = converter.candidateWidgets(buildFormDef(Seq(widget("first"), ignoredWidget("second")), Seq(widget("third"))))
      widgets should have size 2
      widgets.map(_.name) shouldBe Seq("first", "third")
    }
  }

  "convert" - {
    "should include all candidate widgets and their values from the data" in {
      val result = converter.convert(
        Form(
          FormData(Json.obj("firstWidget" -> "valueOne", "secondWidget" -> "valueTwo")),
          buildFormDef(Seq(widget("firstWidget"), widget("secondWidget"))),
          UUID.randomUUID(),
          Instant.now
        ),
        requestInfo
      )
      result shouldBe Row(Seq("valueOne", "valueTwo"))
    }
  }

  "override behaviour" - {
    "should allow columns to be added by overriding columnValues" in {
      val result = new DateAwareGeneralConverter {
        override val columnValues: (FormData, FormDefinition, RequestInfo) => Seq[String] =
          super.columnValues(_, _, _) :+ "myNewValue"
      }.convert(
        Form(
          FormData(Json.obj("onlyWidget" -> "onlyValue")),
          buildFormDef(Seq(widget("onlyWidget"))),
          UUID.randomUUID(),
          Instant.now
        ),
        requestInfo
      )
      result shouldBe Row(Seq("onlyValue", "myNewValue"))
    }
  }

  val requestInfo = RequestInfo("somehost", "somepath", Map.empty, Map.empty, Seq.empty, "", Locale.UK)

  def buildSection(sectionWidgets: Seq[Widget]): Section = new Section {

    override val name: String = UUID.randomUUID().toString
    override val widgets: Seq[Widget] = sectionWidgets
    override val title: String = ""
    override val submissions: Seq[DataTransformer] = Seq.empty
    override val receivers: Seq[DataTransformer] = Seq.empty
    override val redirectBeforeRender: RedirectBeforeRender = NoRedirectBeforeRender
    override val redirectAfterSubmissions: RedirectAfterSubmissions = AlwaysRedirectToForm

    override val canContinue: SectionCanContinue = SectionCanAlwaysContinue

    //noinspection NotImplementedCode
    override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo, runtimeWidgets: Widget*)(implicit executionContext: ExecutionContext): Future[String] = ???
  }

  private def buildFormDef(sectionWidgets: Seq[Widget]*) = new FormDefinition {

    import concurrent.duration._

    override def renderComplete(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String] = Future.successful("")

    override val name: FormDefinitionName = FormDefinitionName("myformdef")
    override val sections: Seq[Section] = sectionWidgets.map(buildSection)
    override val locale: Locale = new Locale("en", "GB")
    override val messages: Messages[_] = Messages.empty()
    override val inflightDataRetention: Duration = 1 second
    override val pages: Seq[Page] = Seq.empty
    override val resumePages: Seq[Page] = Seq.empty
    override val submissions: Seq[DataTransformer] = Seq.empty
  }

  private def ignoredWidget(widgetName: String) = new Widget with Named {
    override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] =
      Future.successful("")

    override val name: String = widgetName
  }

  private def widget(widgetName: String): Widget = new Widget with Named with Persisted {

    override val name: String = widgetName

    override def validate(data: FormData)(implicit ec: ExecutionContext): Future[ValidationResult] = Future.successful(ValidationResult(data, Seq.empty))

    override def merge(formPostData: Map[String, Seq[String]], existingFormData: FormData): FormData = existingFormData

    override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] =
      Future.successful("")
  }

  private def dateWidget(widgetName: String) = new DateWidget {
    override val constraints: Set[Constraint] = Set.empty
    override val reportingName: String = widgetName

    override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = Future.successful("")

    override val name: String = widgetName
  }


}
