# UX Forms Google Spreadsheets Submission Connector

Add this library to your form definition to submit its data to a Google Spreadsheet.



## Set up a Google API key

https://console.cloud.google.com

In the top left of your window, select the drop drop down then "New Project"
Give it a reasonable name.

Wait for it to be created.

Select it in the top left drop-down.

Enable the Google sheets API
* Getting Started -> Explore and enable APIs -> Enable APis and Services 
 -> Search for "sheets" -> Select "Google Sheets API" -> Enable

Credentials -> Create Credentials -> Service Account
* Give it a name specific to your form
-> Create
-> We can ignore the Service Account Permission step as well as the Permissions step.

Create Key: Json
Save the file to your project's src/main/resources folder

Note: If you want to avoid committing this file to your repo you can instead inject the file at build time with your
CI tool.


## Add the library to your form's project

in your build.sbt:

`libraryDependencies ++= Seq(
  "com.uxforms" %% "uxforms-google-spreadsheets" % "16.0.0"
)`

## Set up the submission in your form

```
class SubmitToGoogle(implicit classLoader: ClassLoader) extends GoogleSpreadsheetSubmission(
  classLoader.getResourceAsStream("/downloaded-credential-file.json"),
  "GOOGLE_SHEET_ID",
  new DateAwareGeneralConverter().convert
)
```

## Grant permissions on your spreadsheet to your new Service Account

Where the Google sheet id can be found in the url of the sheet.
E.g. For https://docs.google.com/spreadsheets/d/5AfDMClAWJUCiEN8Oogsyx_UWphEd5t_UBIkY6z4Qgt7/edit#gid=0
The Id is between /d/ and /edit, so the id of this sheet is `5AfDMClAWJUCiEN8Oogsyx_UWphEd5t_UBIkY6z4Qgt7`


Look in your downloaded-credentials-file.json and you'll see a line `client_email`. Take that email address and add that 
as an address that the spreadsheet is shared with. Make sure you grant edit rights.

