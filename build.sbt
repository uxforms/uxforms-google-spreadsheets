
name := """uxforms-google-spreadsheets"""

scalaVersion := "2.11.7"

organization := "com.uxforms"

enablePlugins(SbtOsgi)

resolvers += "uxforms-public-download" at "https://artifacts-public.uxforms.net"

libraryDependencies ++= Seq(
  "com.uxforms" %% "uxforms-dsl" % "15.25.0" % "provided",

  "org.slf4j" % "slf4j-api" % "1.7.21",
  "com.google.apis" % "google-api-services-sheets" % "v4-rev20191115-1.30.3",
  "com.google.auth" % "google-auth-library-oauth2-http" % "0.18.0",
  "com.chuusai" %% "shapeless" % "2.1.0",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "org.mockito" % "mockito-core" % "1.10.19" % "test"
)

dependencyOverrides ++= Seq(
  "com.fasterxml.jackson.core" % "jackson-core" % "2.6.5",
  "com.google.guava" % "guava" % "28.1-android"
)

//shellPrompt := { state => "[" + scala.Console.CYAN + name.value + scala.Console.RESET + "] $ " }

// OSGi

osgiSettings

val embeddedJarNames = Seq(
  "google-api",
  "google-auth",
  "google-oauth",
  "google-http",
  "guava",
  "error_prone_annotations",
  "opencensus",
  "checker-compat",
  "grpc",
  "jsr305",
  "httpclient",
  "commons-codec",
  "commons-logging",
  "jetty"
)

OsgiKeys.embeddedJars := (Compile / Keys.externalDependencyClasspath).value map (_.data) filter (f => embeddedJarNames.exists(jarName => f.getName.startsWith(jarName)))

OsgiKeys.exportPackage := Seq("com.uxforms.submission.googlespreadsheet")

OsgiKeys.importPackage := Seq(
  "!org.apache.avalon.*",
  "!org.apache.http.*",
  "!org.apache.log.*",
  "!org.apache.log4j.*",
  "!com.google.common.util.concurrent.internal",
  "!com.google.appengine.*",
  "!com.google.apphosting.api.*",
  "!io.grpc.override.*",
  "!sun.misc.*",
  "*"
)

OsgiKeys.privatePackage := Seq("com.uxforms.submission.googlespreadsheet.internal")


// Release and publishing

publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))

val osgiBundle = (ref: ProjectRef) => ReleaseStep(
  action = releaseStepTask(OsgiKeys.bundle)
)

releaseProcess := (thisProjectRef apply { ref =>
  import ReleaseTransformations._
  Seq[ReleaseStep](
    checkSnapshotDependencies,
    inquireVersions,
    runClean,
    osgiBundle(ref), // insert this into the default release step order as runTest seems to ignore the dependsOn hooks below.
    runTest,
    setReleaseVersion,
    commitReleaseVersion,
    tagRelease,
    publishArtifacts,
    setNextVersion,
    commitNextVersion,
    pushChanges
  )
}).value
